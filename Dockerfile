FROM registry.access.redhat.com/ubi8/php-74

# Add application sources
ADD . .

USER root

RUN mv --force ./bin/* /usr/local/bin/ && \
    # Create a tmp folder where to put temporarily custom plugins, themes, etc.
    mkdir -p /opt/app-root/downloads/{plugins,themes} && \
    # Move plugins, themes, etc to a temp folder
    mv ./plugins/* /opt/app-root/downloads/plugins/ && \
    mv ./themes/* /opt/app-root/downloads/themes/ && \
    # Install WP-CLI command line tool
    # Will allow to create users: 
    #   wp user create admin email@cern.ch --role=administrator
    #     will create admin user with random password
    # Must run from the WP installation directory (/opt/app-root/src)
    # https://make.wordpress.org/cli/handbook/installing/
    curl -Ls https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /opt/app-root/downloads/wp-cli.phar && \
    # Verify it's ok
    php /opt/app-root/downloads/wp-cli.phar --info && \
    mv --force /opt/app-root/downloads/wp-cli.phar /usr/local/bin/wp && \
    # Make all scripts under /usr/local/bin/ executables
    chmod +x /usr/local/bin/* && \    
    # Fix permissions
    fix-permissions /opt/app-root/ && \    
    # Remove leftovers
    rm -rf .git/
    
USER default
# Run script uses standard ways to configure the PHP application
# and execs httpd -D FOREGROUND at the end
# See more in <version>/s2i/bin/run in this repository.
# Shortly what the run script does: The httpd daemon and php needs to be
# configured, so this script prepares the configuration based on the container
# parameters (e.g. available memory) and puts the configuration files into
# the approriate places.
# This can obviously be done differently, and in that case, the final CMD
# should be set to "CMD httpd -D FOREGROUND" instead.
CMD ["/usr/local/bin/run.sh"]